# PleaseBanMe

Auto-bans players if they have a specific permission, which mitigates some backdoor/account compromised attacks.

---

When an attacker logs into a vulnerable server, one of the first things they do is give themselves
permissions, so that they can carry on their attack. An easy way to give someone all permissions is
through a wildcard like LuckPerms' `*` "all permissions" wildcard. This gives the attacker all
permissions, regardless of if they really want a specific permission or not.

PBM takes advantage of this, and sets up a "honeypot" permission (defined by the server owner)
which, if a player is detected with, will ban them. The plugin can be configured to make the
disconnection message seem legitimate e.g. as if the server has crashed, or if they have been
timed out.

## Paper

PBM is exposed as a Paper plugin, with its own configuration file.

### Dependencies

* [Java >=16](https://adoptopenjdk.net/?variant=openjdk16&jvmVariant=hotspot)
* [Paper >=1.13](https://papermc.io/)

### [Download](https://gitlab.com/api/v4/projects/28038640/jobs/artifacts/master/raw/paper/build/libs/please-ban-me-paper-1.0.jar?job=build)

### Documentation

The plugin is configured through `config.yml`. The description of each config can be found there.

### Permissions

* `pbm.op` Dummy permission - true if the player is an op.
* `pbm.no-op` Dummy permission - never true unless manually set through e.g. LuckPerms `*`.
* `pbm.command.reload` Gives permission to run `/pbm reload`.

### Commands

Root command: `/please-ban-me`, `/pbm`

* `reload` Reloads the plugin configuration file
