package com.gitlab.aecsocket.pleasebanme;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class BanFeature implements Listener {
    private final PleaseBanMePlugin plugin;

    public BanFeature(PleaseBanMePlugin plugin) {
        this.plugin = plugin;
    }

    public PleaseBanMePlugin plugin() { return plugin; }

    public boolean shouldBan(Player player) {
        for (String permission : plugin.permissions()) {
            if (player.hasPermission(permission))
                return true;
        }
        return false;
    }

    public void ban(Player player) {
        player.kick(plugin.message());
        player.banPlayer(PleaseBanMePlugin.BAN_MESSAGE);
        if (plugin.ipBan())
            player.banPlayerIP(PleaseBanMePlugin.BAN_MESSAGE);
    }

    @EventHandler
    private void event(PlayerLoginEvent event) {
        if (shouldBan(event.getPlayer())) {
            event.disallow(plugin.kickType(), plugin.message());
            ban(event.getPlayer());
        }
    }


    public void check() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (shouldBan(player))
                ban(player);
        }
    }
}
