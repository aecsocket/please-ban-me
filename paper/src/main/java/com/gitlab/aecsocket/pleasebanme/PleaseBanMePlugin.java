package com.gitlab.aecsocket.pleasebanme;

import cloud.commandframework.ArgumentDescription;
import cloud.commandframework.Command;
import cloud.commandframework.execution.CommandExecutionCoordinator;
import cloud.commandframework.paper.PaperCommandManager;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.function.Function;

public class PleaseBanMePlugin extends JavaPlugin {
    public static final String BAN_MESSAGE = "PBM_Check";

    private int checkInterval;
    private List<String> permissions;
    private PlayerLoginEvent.Result kickType;
    private Component message;
    private boolean ipBan;

    private int taskId = -1;
    private final BanFeature banFeature = new BanFeature(this);

    @Override
    public void onEnable() {
        saveDefaultConfig();
        Bukkit.getPluginManager().registerEvents(new BanFeature(this), this);
        try {
            PaperCommandManager<CommandSender> commandManager = new PaperCommandManager<>(this,
                    CommandExecutionCoordinator.simpleCoordinator(),
                    Function.identity(), Function.identity());

            Command.Builder<CommandSender> root = commandManager.commandBuilder("please-ban-me",
                    ArgumentDescription.of("PleaseBanMe plugin commands"), "pbm")
                    .permission("pbm.command");
            commandManager.command(root
                    .literal("reload", ArgumentDescription.of("Reloads plugin configuration."))
                    .permission("pbm.command.reload")
                    .handler(ctx -> {
                        reload();
                        ctx.getSender().sendMessage(Component.text("Reloaded configs."));
                    }));
        } catch (Exception e) {
            getLogger().warning("Could not set up command manager");
            e.printStackTrace();
        }
        reload();
    }

    public int checkInterval() { return checkInterval; }
    public List<String> permissions() { return permissions; }
    public PlayerLoginEvent.Result kickType() { return kickType; }
    public Component message() { return message; }
    public boolean ipBan() { return ipBan; }

    public BanFeature banFeature() { return banFeature; }

    public void loadConfig() {
        Configuration config = getConfig();
        checkInterval = (int) (config.getDouble("check-interval", 20) * 20);
        permissions = config.getStringList("permissions");
        kickType = config.getObject("kick-type", PlayerLoginEvent.Result.class, PlayerLoginEvent.Result.KICK_BANNED);
        message = MiniMessage.get().parse(config.getString("message", "Disconnected"));
        ipBan = config.getBoolean("ip-ban", true);
    }

    public void reload() {
        loadConfig();
        if (taskId > -1)
            Bukkit.getScheduler().cancelTask(taskId);
        if (checkInterval > 0)
            taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, banFeature::check, 0, checkInterval);
    }
}
